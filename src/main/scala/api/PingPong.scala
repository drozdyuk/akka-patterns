/**
  * API pattern wraps the actors behind a synchronous api,
  * simplifying the system usage by not making the users
  * care about the exact protocol between actors.
  */
package api
import akka.pattern.{ask, pipe}
import akka.actor.{ActorRef, Props, Actor}
import akka.util.Timeout

import scala.concurrent.Await
import scala.concurrent.duration._


object PingPong {
  case object Ping
  case object Pong
  implicit val timeout:Timeout = 3 second

  def apply():Props = Props(new PingPong)

  def ping(pid: ActorRef) = {
    val f = (pid ? Ping).mapTo[Pong.type]
    Await.result(f, 3 second)
    f.value.get.get
  }
}

/**
  * Receives a Ping message, waits 1 second, and replies with a Pong message.
  */
class PingPong extends Actor{
  import PingPong._
  override def receive: Receive = {
    case Ping =>
      Thread.sleep(500)
      sender ! Pong
  }
}
