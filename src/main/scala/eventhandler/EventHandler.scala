package eventhandler

import akka.actor._
import scala.util.Try

object EventHandler {
  case class Add(function: Any=>Any)
  case class Event(event:Any)

  def apply():Props = Props(new EventHandler)

  def make(name:String)(implicit system:ActorSystem) = {
    system.actorOf(EventHandler(), name)
  }
  def addHandler(name:String, function:Any=>Any)(implicit system: ActorSystem) = {
    system.actorSelection("/user/" + name) ! Add(function)
  }
  def event(name:String, x:Any)(implicit system:ActorSystem) = {
    system.actorSelection("/user/" + name) ! Event(x)
  }
}
class EventHandler extends Actor {
  import EventHandler._

  def noOp(x:Any) = ()

  override def receive = handler(_=>())

  def handler(function: Any=>Any): Receive = {
    case Add(fun) => context.become(handler(fun))
    case Event(evt) => Try(function(evt))
  }
}
