package eventhandler
import akka.actor.ActorSystem

object MotorController {
  case object TooHot
  def addEventHandler()(implicit system: ActorSystem) = {
    EventHandler.addHandler("errors", controller)
  }
  def controller(evt:Any) = {
    evt match {
      case TooHot => println("Turn off motor.")
      case _ => println(s"Ignored event: $evt")
    }
  }
}