package eventhandler

import akka.actor.ActorSystem

object Main extends App {
  import MotorController._
  implicit val system = ActorSystem()

  // Create an event handler and generate an error
  EventHandler.make("errors")
  EventHandler.event("errors", "hi")
  MotorController.addEventHandler()
  EventHandler.event("errors", "cool")
  EventHandler.event("errors", TooHot)
}

