package api

import akka.actor.ActorSystem

import scala.concurrent.{Await}
import scala.concurrent.duration._
object PingPongTest extends App{
  import PingPong._
  val system = ActorSystem()
  val pingPong =system.actorOf(PingPong())

  val t0 = System.nanoTime()

  val r = PingPong.ping(pingPong)
  val r2 = PingPong.ping(pingPong)
  val r3 = PingPong.ping(pingPong)
  val r4 = PingPong.ping(pingPong)

  assert(r == Pong)
  assert(r2 == Pong)
  assert(r3 == Pong)
  assert(r4 == Pong)
  val t1 = System.nanoTime()
  Await.ready(system.terminate(), 1 second)

  println("Notice that Elapsed time is the sum of all the executions: " + ((t1 - t0) / Math.pow(10,9)) + "s")
}
